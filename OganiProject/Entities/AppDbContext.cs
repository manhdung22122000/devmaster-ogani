using Microsoft.EntityFrameworkCore;

namespace OganiProject.Entities;

public class AppDbContext: DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options)
        :base(options)
    {
        
    }

    public DbSet<CategoryEntity> CategoryEntities { get; set; }
}