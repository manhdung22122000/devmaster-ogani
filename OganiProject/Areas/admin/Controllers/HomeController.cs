using Microsoft.AspNetCore.Mvc;

namespace OganiProject.Areas.admin.Controllers;

[Area("admin")]
public class HomeController : Controller
{
    // GET
    public IActionResult Index()
    {
        return View();
    }
}