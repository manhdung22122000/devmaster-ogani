using Microsoft.AspNetCore.Mvc;
using OganiProject.Entities;
using OganiProject.Models;

namespace OganiProject.Areas.admin.Controllers;

[Area("admin")]
public class CategoryController : Controller
{
    private readonly IWebHostEnvironment _environment;
    private readonly AppDbContext _dbContext;

    public CategoryController(IWebHostEnvironment environment,
        AppDbContext dbContext)
    {
        _environment = environment;
        _dbContext = dbContext;
    }
    public IActionResult Add()
    {
        return View();
    }

    [HttpPost]
    public IActionResult UploadImage(IFormFile imageUpload)
    {
        if (imageUpload == null)
            return Json(new FileUpload()
            {
                Status = "error",
                Message = "File không tồn tại"
            });

        var fullPath = Path.Combine(_environment.WebRootPath, "upload", imageUpload.FileName);
        using(var fileStream = new FileStream(fullPath, FileMode.Create))
        {
            imageUpload.CopyTo(fileStream);
        }

        return Json(new FileUpload()
        {
            FileName = imageUpload.FileName,
            FilePath = Path.Combine("/upload", imageUpload.FileName),
            Status = "success",
            Message = "Upload file thành công!"
        });
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult GetList()
    {
        var data = _dbContext.CategoryEntities
            .Where(x => x.IsDeleted == false)
            .ToList();
        return Ok(data);
    }

    [HttpPost]
    public IActionResult AddCategory([FromBody] Category model)
    {
        if(model.Id > 0)
        {
            var entity = _dbContext.CategoryEntities.Find(model.Id);
            if(entity != null)
            {
                entity.Name = model.Name;
                entity.Slug = model.Slug;
                entity.Description = model.Description;
                entity.UpdatedDate = DateTime.Now;
                _dbContext.CategoryEntities.Update(entity);
            }
        }
        else
        {
            var entity = new CategoryEntity()
            {
                Name = model.Name,
                Slug = model.Slug,
                Description = model.Description,
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now,
                IsActive = true
            };
            _dbContext.CategoryEntities.Add(entity);
        }
        
        var status = _dbContext.SaveChanges();
        return Ok(status);
    }

    public class FileUpload
    {
        public string FileName { get; set; }

        public string FilePath { get; set; }

        public string Status { get; set; }

        public string Message { get; set; }
    }

}