﻿using System;
namespace OganiProject.Models
{
	public class Category
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Slug { get; set; }

		public string Description { get; set; }

		public string Image { get; set; }
	}

}

