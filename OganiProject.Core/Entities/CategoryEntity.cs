﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OganiProject.Core.Entities
{
    [Table("Category")]
    public class CategoryEntity : BaseEntity
    {
        public string Name { get; set; }

        public string Slug { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public int N { get; set; }

        public int? a { get; set; }

        public DateTime dt { get; set; }

        public DateTime? dtn { get; set; }
    }
}

