﻿using System;
using Microsoft.EntityFrameworkCore;
using OganiProject.Core.Entities;

namespace OganiProject.Core.Initial
{
	public class OganiAppDbContext: DbContext 
	{
		public OganiAppDbContext(DbContextOptions<OganiAppDbContext> options)
			:base(options)
		{

		}

		public DbSet<CategoryEntity> CategoryEntities { get; set; }
	}
}

