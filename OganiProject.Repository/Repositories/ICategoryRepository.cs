﻿using System;
using OganiProject.Core.Entities;

namespace OganiProject.Repository.Repositories
{
	public interface ICategoryRepository
	{
		int Insert(CategoryEntity entity);

		int Update(CategoryEntity entity);

		int DeleteById(int id);

		List<CategoryEntity> GetAll();

		List<CategoryEntity> GetByName(string name);
	}
}

