﻿using System;
using OganiProject.Core.Entities;
using OganiProject.Core.Initial;

namespace OganiProject.Repository.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly OganiAppDbContext _dbContext;

        public CategoryRepository(OganiAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

       

        public int DeleteById(int id)
        {
            throw new NotImplementedException();
        }

        public List<CategoryEntity> GetAll()
        {
            return _dbContext
                .CategoryEntities
                .Where(x => x.IsDeleted == false)
                .ToList();
        }

        public List<CategoryEntity> GetByName(string name)
        {
            return _dbContext
                .CategoryEntities
                .Where(x => x.Name.ToLower() == name.ToLower() &&
                            x.IsDeleted == false)
                .ToList();
        }

        public int Insert(CategoryEntity entity)
        {
            _dbContext.CategoryEntities.Add(entity);
            return _dbContext.SaveChanges();
        }

        public int Update(CategoryEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}

