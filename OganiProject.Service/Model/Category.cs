﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OganiProject.Service.Model
{
	public class Category
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Image { get; set; }

	
		public int OrderId { get; set; }
	}
}

