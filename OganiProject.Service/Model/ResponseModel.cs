﻿using System;
namespace OganiProject.Service.Model
{
	public class ResponseModel
	{
		public int Status { get; set; }

		public string StatusMessage { get; set; }
	}
}

