﻿using System;
using OganiProject.Service.Model;

namespace OganiProject.Service.Services
{
	public interface ICategoryService
	{
        ResponseModel Add(Category model);
    }
}

