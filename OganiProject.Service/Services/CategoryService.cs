﻿using System;
using OganiProject.Core.Entities;
using OganiProject.Repository.Repositories;
using OganiProject.Service.Model;

namespace OganiProject.Service.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository; 
        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public ResponseModel Add(Category model)
        {
            // kiểm tra xem tên có bị trùng không
            var checkEntity = _categoryRepository.GetByName(model.Name);
            if (checkEntity.Any())
            {
                return new ResponseModel
                {
                    Status = 400,
                    StatusMessage = "Tên danh mục bị trùng"
                };
            }
            // thêm mới
            var entity = new CategoryEntity()
            {
                Name = model.Name,
                Slug = model.Name, // có hàm tạo slug riêng
                CreatedBy = "",
                UpdatedBy = "",
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now
            };

            var status = _categoryRepository.Insert(entity);
            if(status == 0)
            {
                return new ResponseModel
                {
                    Status = 500,
                    StatusMessage = "Lỗi hệ thống"
                };
            }

            return new ResponseModel
            {
                Status = 200,
                StatusMessage = "Thêm mới danh mục thành công"
            };
        }
    }
} 

