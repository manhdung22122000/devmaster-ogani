﻿using Microsoft.EntityFrameworkCore;
using OganiProject.Core.Initial;
using OganiProject.Repository.Repositories;
using OganiProject.Service.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

var connectionString =
    builder.Configuration.GetConnectionString("Default");

builder.Services.AddDbContext<OganiAppDbContext>(
    options => options.UseSqlServer(connectionString));

builder.Services.AddScoped<ICategoryService, CategoryService>();
builder.Services.AddScoped<ICategoryRepository, CategoryRepository>();


var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

