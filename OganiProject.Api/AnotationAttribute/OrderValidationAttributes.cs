﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OganiProject.Api.AnotationAttribute
{
	public class OrderValidationAttribute: ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var orderId = (int)value;
            if(orderId <= 0)
            {
                this.ErrorMessage = "Số thứ tự phải lớn hơn 0";
                return false;
            }

            return true;
        }
    }


    public class RequiredAttributes : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            
            if(value == null)
            {
                this.ErrorMessage = "Không được để trống!"; 
                return false;
            }

            return true;
        }
    }
}

