﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OganiProject.Api.RequestModel;
using OganiProject.Service.Model;
using OganiProject.Service.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OganiProject.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpPost(nameof(Add))]
        public IActionResult Add(CategoryRequestModel model)
        {
            // model mapping
            // data atribution
            //var res = _categoryService.Add(model);
            return Ok();
        }
    }
}

