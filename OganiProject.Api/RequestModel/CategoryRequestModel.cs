﻿using System;
using OganiProject.Api.AnotationAttribute;

namespace OganiProject.Api.RequestModel
{
	public class CategoryRequestModel
	{
		public string Name { get; set; }

		public int OrderId { get; set; }
	}
}

